# iOS Commerce App with Parse Cloud & Stripe
> It consists of two apps: Admin and Buyer. The back-end was built on Parse.
> Admin app has a dashboard where administrators can manage the buyers and goods.
> Buyer app is just for consumers to purchase goods.
> Customized stripe node.js library has been integrated in the back-end for server-side payment processing.

## Main Features & Skills
* Parse.com as a back-end & Cloud Code
* Payment processing with Stripe, Remote push notifciation
* Credit card scan by Card.IO
* GPS tracking, Facebook Sign up, Barcode/QRCode scan
* Simple messaging between adminstrators and buyers
* Simple Linie graph

## ER Diagram of Models
![Entity-Relationship Diagram](DataModel/ERDiagram.jpg)