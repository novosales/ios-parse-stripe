//
//  FMBMessageListVC.h
//  eCommerceBuyer
//
//  Created by Donovan on 9/8/14.
//  Copyright (c) 2014 Donovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface FMBMessageListVC : UITableViewController

// --------------------------------------------------------------------------------------
@property (strong, 	nonatomic)      PFImageView *imageviewBackground;

@end
