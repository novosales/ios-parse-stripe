//
//  FMAChooseColorCVCell.h
//  eCommerceManager
//
//  Created by Donovan on 9/3/14.
//  Copyright (c) 2014 Donovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FMAChooseColorCVCell : UICollectionViewCell

// -----------------------------------------------------------------------------------
@property (weak, nonatomic) IBOutlet UILabel     *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel     *labelBack;
@property (weak, nonatomic) IBOutlet UIButton    *btnChecked;

// -----------------------------------------------------------------------------------
#pragma mark - Main Functions
- (void)configureCellWithData:(id)data;

@end
