//
//  FMASecurityCamerasCVCell.h
//  eCommerceManager
//
//  Created by Donovan on 8/25/14.
//  Copyright (c) 2014 Donovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FMASecurityCamerasCVCell : UICollectionViewCell

// -----------------------------------------------------------------------------------
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

// -----------------------------------------------------------------------------------
#pragma mark - Main Functions
- (void)configureCellWithData:(id)data;

@end
